#! /bin/bash

### Install docker https://www.cloudbooklet.com/how-to-install-docker-on-ubuntu-22-04/
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt-cache policy docker-ce
sudo apt install docker-ce -y
sudo usermod -aG docker $USER

### Install Nginx Proxy Manager
sudo docker run -d \
    --name=npm \
    --restart unless-stopped \
    -p 8181:8181 \
    -p 80:8080 \
    -p 443:4443 \
    -v /docker/config/npm:/config:rw \
    jlesage/nginx-proxy-manager:latest

### Create screenshot docker image
sudo curl https://gitlab.com/syn0l0gy/screenshotv3/-/archive/main/screenshotv3-main.tar -o /home/ubuntu/screenshotv3-app-main.tar
sudo tar -xvf screenshotv3-app-main.tar -C .
cd screenshotv3-main
sudo docker build -t screenshot .
sudo docker run -d --init --name screenshot --restart unless-stopped -p 4000:4000 screenshot

### Configure lazydocker
if ! grep -q lazydocker /home/ubuntu/.bashrc; then
  echo "alias lazydocker='sudo docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock -v /docker/config/lazydocker:/.config/jesseduffield/lazydocker lazyteam/lazydocker'" >> /home/ubuntu/.bashrc
fi

sudo docker pull lazyteam/lazydocker

### DDNS Update on reboot
(crontab -l 2>/dev/null; echo "@reboot /usr/bin/sleep 60; /usr/bin/curl --user <domain>:<token> https://update.dedyn.io/") | crontab -

### Create Docker custom bridge network
sudo docker network create -d bridge custom_bridge
sudo docker network connect custom_bridge npm
sudo docker network connect custom_bridge screenshot

### Finished
echo "********************************************"
echo "1. Update crontab with DDNS domain and token"
echo "2. Configure Nginx Network Proxy Manager"
echo "********************************************"
